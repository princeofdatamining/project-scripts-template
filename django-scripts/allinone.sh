#!/bin/bash
# 一键部署脚本

anynowtime="date +'%Y-%m-%d %H:%M:%S'"
NOW="echo [\`$anynowtime\`][PID:$$]"

##### 可在脚本开始运行时调用，打印当时的时间戳及PID。
function job_start
{
    echo "`eval $NOW` job_start"
}

##### 可在脚本执行成功的逻辑分支处调用，打印当时的时间戳及PID。 
function job_success
{
    MSG="$*"
    echo "`eval $NOW` job_success:[$MSG]"
    exit 0
}

##### 可在脚本执行失败的逻辑分支处调用，打印当时的时间戳及PID。
function job_fail
{
    MSG="$*"
    echo "`eval $NOW` job_fail:[$MSG]"
    exit 1
}

job_start

###### 可在此处开始编写您的脚本逻辑代码
###### 作业平台中执行脚本成功和失败的标准只取决于脚本最后一条执行语句的返回值
###### 如果返回值为0，则认为此脚本执行成功，如果非0，则认为脚本执行失败

###### .gitrc ######
# 项目 Git 仓库(本机需要公钥并在 git 服务中登记)
PROJ_GIT_URL=git@github.com:USERNAME/REPO.git
# 项目 Git 版本分支
PROJ_GIT_BRANCH=master
# 项目 Git 目录(目录、权限依赖于运维或开发规范，不在应用中涉及)
PROJ_GIT_DIR=/work/projs/GROUP/PROJ/src
###### .gitrc ######

###### .projrc ######
# 项目代号
PROJ_CODENAME=sample

# 如果需要 pyenv 则需要设置：python 版本、虚拟环境名、pyenv 虚拟环境目录；
# 否则可跳过
PROJ_PYTHON_VER=3.6.6
PROJ_PYTHON_ENV=$PROJ_CODENAME
PROJ_PYTHON_BIN="/work/pyenv/versions/$PROJ_PYTHON_ENV/bin"

# 如果不使用 pyenv，则需要设置对应的 python、pip 路径；
# 否则可跳过
PROJ_PYTHON="$PROJ_PYTHON_BIN/python"
PROJ_PIP="$PROJ_PYTHON_BIN/pip"

# 数据库连接配置
DB_URI="mysql:///db_$PROJ_CODENAME"

# 静态资源 URL 及目录
STATIC_URL=/be/static/
STATIC_ROOT="/work/projs/GROUP/$PROJ_CODENAME/web"

# 日志
PROJ_LOG_DIR="/work/logs/GROUP/$PROJ_CODENAME"
PROG_LOG_FILE="$PROJ_LOG_DIR/root.log"

# raven(sentry)
RAVEN_DSN=""

# 容器运行端口
PROJ_WEB_PORT=8000
# 前端代码目录
PUB_WWW_ROOT="$PROJ_GIT_DIR/www/default/dist"
# 域名
PROJ_DOMAIN="$PROJ_CODENAME.work.com"

###### .projrc ######

# 克隆/更新 git
echo "##### Update / clone from git ..."
if [ ! -d $PROJ_GIT_DIR ]; then
    mkdir -p $PROJ_GIT_DIR && git clone --recursive -b $PROJ_GIT_BRANCH $PROJ_GIT_URL $PROJ_GIT_DIR
    cd $PROJ_GIT_DIR
else
    cd $PROJ_GIT_DIR
    . scripts/host/01-git-update.sh
fi

# 更新项目配置，保证 scripts 下所有脚本均可执行
echo "##### touch .gitrc & .projrc ..."
. scripts/base/00-gitrc.sh
. scripts/base/01-projrc.sh

# 创建 pyenv 虚拟环境；如无需 pyenv，则注释
echo "##### check python / pyenv ..."
. scripts/host/02-pyenv.sh

# 安装依赖
echo "##### check libraries ..."
. scripts/host/02-pip.sh
. scripts/base/02-pip.sh
. scripts/dev/40-submodules.sh

# 更新项目配置
echo "##### update environ ..."
mkdir -p $PROJ_LOG_DIR
. scripts/base/03-environ.sh
. scripts/base/10-prepare.sh

# 运行服务: uwsgi, supervisor, nginx
echo "##### update uwsgi & supervisor & nginx configuration"
. scripts/host/19-uwsgi.sh
. scripts/host/19-supervisor.sh
. scripts/host/19-nginx.sh

# 【通用】模板及资源
echo "##### update / clone resources & templates ..."
if [ ! -d /work/resources.git ]; then
    git clone -b v1 git@github.com:USERNAME/resources.git /work/resources.git
else
    echo $(cd /work/resources.git; git pull) 
fi


echo "##### update & reload supervisor ..."
SUPERVISOR_CONF=/work/conf/supervisor/supervisord.conf
if [ ! -f $SUPERVISOR_CONF ]; then
    job_fail "Not found: $SUPERVISOR_CONF"
fi
SUPERVISOR_CONF_D="/work/conf/supervisor/conf.d"
rm -f $SUPERVISOR_CONF_D/$PROJ_CODENAME.conf
ln -s $PROJ_GIT_DIR/scripts/host/supervisor.conf $SUPERVISOR_CONF_D/$PROJ_CODENAME.conf
echo "##### reload supervisor ..."
[ 0 -eq `ps aux | grep supervisord | grep -v grep | wc -l` ] && supervisord
supervisorctl restart $PROJ_CODENAME


echo "##### update & reload nginx ..."
NGINX_CONF=/work/conf/nginx/nginx.conf
if [ ! -f $NGINX_CONF ]; then
    job_fail "Not found: $NGINX_CONF"
fi
NGINX_CONF_D="/work/conf/nginx/conf.d"
rm -f $NGINX_CONF_D/$PROJ_CODENAME.conf
ln -s $PROJ_GIT_DIR/scripts/host/nginx.conf $NGINX_CONF_D/$PROJ_CODENAME.conf
echo "##### reload nginx ..."
if [ 0 -eq `ps aux | grep nginx | grep -v grep | wc -l` ]; then
    nginx
else
    nginx -s reload
fi

job_success
