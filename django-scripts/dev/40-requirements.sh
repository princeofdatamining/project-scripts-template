# 环境变量
. .projrc

# 工作目录
cd $PROJ_GIT_DIR

# 按蓝鲸要求生成 requirements.txt
cat scripts/base/requirements.txt >  requirements.txt
echo "" >> requirements.txt
cat scripts/host/requirements.txt >> requirements.txt
