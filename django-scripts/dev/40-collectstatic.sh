# 环境变量
. .projrc

# 工作目录
cd $PROJ_GIT_DIR

# 收集静态资源存至指定目录(提交前处理)
$PROJ_PYTHON manage.py collectstatic --noinput
