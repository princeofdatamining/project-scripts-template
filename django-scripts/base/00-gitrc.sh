echo "" > .gitrc && cat <<EOF >> .gitrc
# 项目 Git 仓库(本机需要公钥并在 git 服务中登记)
PROJ_GIT_URL=$PROJ_GIT_URL
# 项目 Git 版本分支
PROJ_GIT_BRANCH=$PROJ_GIT_URL
# 项目 Git 目录(目录、权限依赖于运维或开发规范，不在应用中涉及)
PROJ_GIT_DIR=$PROJ_GIT_DIR
EOF