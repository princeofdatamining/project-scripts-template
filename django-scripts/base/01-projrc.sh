echo "" > .projrc && cat <<EOF >> .projrc
# 引入 git 配置项
. .gitrc

# 项目代号
PROJ_CODENAME=$PROJ_CODENAME

# 如果需要 pyenv 则需要设置：python 版本、虚拟环境名、pyenv 虚拟环境目录；
# 否则可跳过
PROJ_PYTHON_VER=$PROJ_PYTHON_VER
PROJ_PYTHON_ENV=$PROJ_PYTHON_ENV
PROJ_PYTHON_BIN=$PROJ_PYTHON_BIN

# 如果不使用 pyenv，则需要设置对应的 python、pip 路径；
# 否则可跳过
PROJ_PYTHON=$PROJ_PYTHON
PROJ_PIP=$PROJ_PIP

# 数据库连接配置
DB_URI="$DB_URI"

# 静态资源 URL 及目录
STATIC_URL="$STATIC_URL"
STATIC_ROOT="$STATIC_ROOT"

# 日志
PROJ_LOG_DIR="$PROJ_LOG_DIR"
PROG_LOG_FILE="$PROG_LOG_FILE"

# raven(sentry)
RAVEN_DSN="$RAVEN_DSN"

# 容器运行端口
PROJ_WEB_PORT=$PROJ_WEB_PORT
# 前端代码目录
PUB_WWW_ROOT="$PUB_WWW_ROOT"
# 域名
PROJ_DOMAIN="$PROJ_DOMAIN"

EOF