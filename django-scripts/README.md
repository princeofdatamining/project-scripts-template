# 部署/开发文档

## 一键部署

- 编辑 `allinone.sh` 中的 `gitrc` 及 `projrc` 相关配置

- 运行脚本
  `./scripts/allinone.sh`

## 脚本说明

- `.gitrc` 代码仓库配置

- `.projrc` 项目环境配置

- `base/`

  - `00-gitrc.sh`
    仅用于一键部署脚本，生成 `.gitrc`
  - `01-projrc.sh`
    仅用于一键部署脚本，生成 `.projrc`
  - `02-pip.sh`
    安装业务功能所需依赖
  - `03-environ.sh`
    生成项目配置
  - `10-prepare.sh`
    预处理数据
  - `20-runserver.sh`
    以 `django runserver` 启动服务
  - `environ.py.sh`
    `Django` 项目配置模板
  - `requirements.txt`
    业务功能所需依赖

- `dev/` # 开发相关，与发布无关
  - `02-pip.sh`
    安装开发所需依赖
  - `40-collectstatic.sh`
    收集所有依赖静态资源（可选）
  - `40-makemigrations.sh`
    检查数据库结构是否变更（提交前检查）
  - `40-pylint.sh`
    语法检查（提交前检查）
  - `40-requirements.sh`
    合并业务、运行所需依赖（可选，提交前检查）
  - `40-submodules.sh`
    以 `python setup.py --develop` 方式注册子模块依赖
  - `40-tests.sh`
    单元测试（提交前处理）
  - `44-migrate.sh`
    直接应用数据库变更，需要数据库高级权限（可选）
  - `env.sh`

    `pyenv` 环境下，无需激活(activate)相关环境

    ```shell
    ./scripts/dev/env.sh pip -V

    ./scripts/dev/env.sh python -V

    ./scripts/dev/env.sh ipython -V
    ```

  - `manage.sh`

    `pyenv` 环境下，无需激活(activate)相关环境

    ```shell
    ./scripts/dev/manage.sh help
    ```

  - `requirements.txt`
    开发所需依赖

- `host/`
  - `00-git-clone.sh`
    仅用于克隆项目，需要准备 `.gitrc` & `.projrc` 配置文件
  - `01-git-update.sh`
    更新
  - `02-pip.sh`
    安装服务运行所需依赖
  - `02-pyenv.sh`
    安装 pyenv 虚拟环境（可选）
  - `19-nginx.sh`
    生成 nginx 配置
  - `19-supervisor.sh`
    生成 supervisor 配置
  - `19-uwsgi.sh`
    生成 uwsgi 配置
  - `20-runserver.sh`
    以自定义方式启动服务: uwsgi, gunicorn ...
  - `requirements.txt`
    服务运行所需依赖
