cat <<EOF > scripts/host/supervisor.conf
[program:$PROJ_CODENAME]
command = $PROJ_PYTHON_BIN/uwsgi --ini $PROJ_GIT_DIR/scripts/host/uwsgi.ini
directory = $PROJ_GIT_DIR
user = $(whoami)
autostart = true
autorestart = true
redirect_stderr = true
stdout_logfile = $PROJ_LOG_DIR/supervisor.log
EOF